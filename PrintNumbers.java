package com.pixerf.utility;
/**
 * 
 * 
 * @author Showkat
 * 02-05-2016
 * 
 * This utility uses Singleton Design Pattern that prints numbers from 1 to 100 that are multiples of 3 or contains 3 
 * and that are multiples of 5 or contains 5
 *
 */
public class PrintNumbers {
	
	 //create an object of PrintNumbers
	 private static PrintNumbers instance = new PrintNumbers();
	 
	//make the constructor private so that this class cannot be
	   //instantiated
	 private PrintNumbers(){}

	   //Get the only object available
	   public static PrintNumbers getInstance(){
	      return instance;
	   }

	   public boolean print(final Integer n){
		   boolean result=false;
		   for(int i=1;i<=n;i++){
				String j=Integer.toString(i);
				
				//check for multiple of both 3 and 5
				if(i%3==0 && i%5==0){
					System.out.println(+i+": is multiple of both 3 and 5");	
					result=true;
				}
				//check for multiple of 3
				else if(i%3==0 ||j.contains("3")){
					System.out.println(+i+": is multiple of 3 or contains 3 Foo");
					result=true;
				}
				//check for multiple of  5
				else if(i%5==0 ||j.contains("5")){
					System.out.println(+i+": is multiple of 5 or contains 5 Bar");
					result=true;
				}
				else 
				{
					System.out.println(i+":");	
					result=false;
				}
				
				
				
				
			}
		   return result;
	   }
		
		}
