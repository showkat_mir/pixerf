package com.pixerf.utility;

import static org.junit.Assert.*;



import java.util.Arrays;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;


@RunWith(Parameterized.class)
public class PrintTest {
	private Boolean expectedValidation;
    private PrintNumbers printNumber;
    private Integer printNum;
    
    @Before
    public void initialize() {
        printNumber = PrintNumbers.getInstance();
    }
    
    public PrintTest(Integer printNum,Boolean expectedValidation) {
   	 this.expectedValidation = expectedValidation;
     this.printNum=printNum;  
   }


    @Parameterized.Parameters
    public static Collection printNumbers() {
        return Arrays.asList(new Object[][] {
                { 1, false },
                {2, false },
                { 3, true },
                { 4, false },
                {5, true }
        });
    }
    
	@Test
	public void testPrint() {
		 assertEquals(expectedValidation,printNumber.print(printNum));
	}

}
